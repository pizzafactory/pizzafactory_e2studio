# About this repository

It contains "PizzaFactory for e2studio" in this repository.

# About "PizzaFactory for e2studio"

PizzaFactory for e2studio is a patch kit for e2studio.

# License

Almost all are licensed under EPL-1.0.

# Development Team

Copyright (C) PizzaFactory for e2studio Development Team

You'll find the name list of team member is in AUTHORS file.

# Trademarks

All brand or product names used in this manual are trademarks or registered trademarks of their respective companies or organisations.
